package br.ifpe.web3.elections.control;

import java.io.Serializable;
import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@ManagedBean(name = "LanguageManager")
@SessionScoped
public class LanguageManager implements Serializable
{
    private static final long serialVersionUID = 2868924339071643569L;
    private Locale locale;

    public LanguageManager()
    {
        this.locale = FacesContext.getCurrentInstance().getExternalContext().
                getRequestLocale();
    }

    public void changeLanguage(Locale locale)
    {
        this.locale = locale;
        FacesContext context = FacesContext.getCurrentInstance();
        context.getViewRoot().setLocale(locale);
    }

    public String english()
    {
        changeLanguage(new Locale("en", "US"));
        return null;
    }

    public String portugues()
    {
        changeLanguage(new Locale("pt", "BR"));
        return null;
    }

    public String deutch()
    {
        changeLanguage(new Locale("de", "DE"));
        return null;
    }

    public Locale getLocale()
    {
        return locale;
    }

    public void setLocale(Locale locale)
    {
        this.locale = locale;
    }
}