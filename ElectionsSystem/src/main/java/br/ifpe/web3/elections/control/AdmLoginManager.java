package br.ifpe.web3.elections.control;

import br.ifpe.web3.elections.model.Administrator;
import java.io.Serializable;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@ManagedBean(name = "AdmLoginManager")
@SessionScoped
public class AdmLoginManager implements Serializable
{
    private static final long serialVersionUID = -1412148965872241766L;

    @ManagedProperty(value = "#{AdministratorsManager}")
    private AdministratorsManager administratorsManager;
    private String login;
    private String name;
    private String password;
    private Boolean logged;

    public AdmLoginManager()
    {
        this.logoff();
    }

    public void logon()
    {
        if (administratorsManager.getList().contains(new Administrator(this.login)))
        {
            Administrator administrator = administratorsManager.getAdminByLogin(this.login);
            if (administrator.isValidPassword(this.password))
            {
                this.name = administrator.getName();
                this.password = "";
                this.logged = true;
            }
            else
            {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                ResourceBundle bundle = ResourceBundle.getBundle("ValidationMessages", facesContext.getViewRoot().getLocale());
                facesContext.addMessage(null, new FacesMessage(bundle.getString("invalid_password")));
            }
        }
        else
        {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ResourceBundle bundle = ResourceBundle.getBundle("ValidationMessages", facesContext.getViewRoot().getLocale());
            facesContext.addMessage(null, new FacesMessage(bundle.getString("admin_doesnt_exist")));
        }
    }

    public final String logoff()
    {
        this.logged = false;
        this.name = "";
        this.login = "";
        this.password = "";
        return "index?faces-redirect=true";
    }

    public Boolean getLogged()
    {
        return logged;
    }

    public void setLogged(Boolean logged)
    {
        this.logged = logged;
    }

    public AdministratorsManager getAdministratorsManager()
    {
        return administratorsManager;
    }

    public void setAdministratorsManager(AdministratorsManager administratorsManager)
    {
        this.administratorsManager = administratorsManager;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}