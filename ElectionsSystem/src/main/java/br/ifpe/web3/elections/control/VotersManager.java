package br.ifpe.web3.elections.control;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

import br.ifpe.web3.elections.model.Voter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@ManagedBean(name = "VotersManager", eager = true)
@ApplicationScoped
public class VotersManager implements Serializable
{
    private static final long serialVersionUID = -7860567896849881646L;
    private List<Voter> list = new ArrayList<>();
    private Voter currentvoter;
    private Boolean editing;

    public VotersManager()
    {
        this.editing = false;
    }

    public String list()
    {
        return "crud_voter?faces-redirect=true";
    }

    public void newVoter()
    {
        this.currentvoter = new Voter();
        this.editing = true;
    }

    public void save()
    {
        if (this.list.contains(this.currentvoter) == false)
        {
            this.list.add(currentvoter);
        }
        this.editing = false;
    }

    public void cancel()
    {
        this.editing = false;
    }

    public void update(Voter voter)
    {
        this.currentvoter = voter;
        this.editing = true;

    }

    public void delete(Voter voter)
    {
        this.list.remove(voter);
    }

    public List<Voter> getList()
    {
        return list;
    }

    public void setList(List<Voter> list)
    {
        this.list = list;
    }

    public Voter getCurrentvoter()
    {
        return currentvoter;
    }

    public void setCurrentvoter(Voter currentvoter)
    {
        this.currentvoter = currentvoter;
    }

    public Boolean getEditing()
    {
        return editing;
    }

    public void setEditing(Boolean editing)
    {
        this.editing = editing;
    }

    Voter getVoterByNumber(Long voterNumber)
    {
        if (this.list.indexOf(new Voter(voterNumber)) >= 0)
        {
            return this.list.get(this.list.indexOf(new Voter(voterNumber)));
        }
        return null;
    }
}