package br.ifpe.web3.elections.utils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class VoterNumberCreator
{
    private static final Random RND = new SecureRandom();

    public static Long createValidVoterNumber()
    {
        List<Integer> digits = new ArrayList<>();
        for (int i = 0; i < 10; ++i)
        {
            digits.add(RND.nextInt(10));
        }

        digits.add(0);
        for (int i = 0; i < 8; ++i)
        {
            digits.set(10, (digits.get(10) + (i + 2) * digits.get(i)) % 11);
        }
        digits.set(10, digits.get(10) >= 10 ? digits.get(10) % 10 : digits.get(10));

        digits.add(((digits.get(8) * 7 + digits.get(9) * 8 + digits.get(10) * 9) % 11) % 10);

        String result = "";
        for (Integer digit : digits)
        {
            result += digit;
        }

        return Long.parseLong(result);
    }
}