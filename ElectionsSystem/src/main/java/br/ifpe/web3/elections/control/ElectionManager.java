package br.ifpe.web3.elections.control;

import br.ifpe.web3.elections.model.Candidate;
import br.ifpe.web3.elections.model.Voter;
import br.ifpe.web3.elections.utils.Counter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@ManagedBean(name = "ElectionManager", eager = true)
@ApplicationScoped
public class ElectionManager implements Serializable
{
    private static final long serialVersionUID = 7855652138297261699L;

    @ManagedProperty(value = "#{CandidatesManager}")
    private CandidatesManager candidatesManager;
    private final Map<Candidate, Counter> pool;
    private List<Voter> alreadyVoted;
    private Boolean isOpen;
    private Integer candidateNumber;
    private Integer totalVotes;

    public ElectionManager()
    {
        this.isOpen = false;
        this.pool = new HashMap<>();
        this.alreadyVoted = new ArrayList<>();
    }

    public void open()
    {
        this.pool.clear();
        for (Candidate candidate : this.candidatesManager.getList())
        {
            this.pool.put(candidate, new Counter());
        }
        this.alreadyVoted.clear();
        this.totalVotes = 0;
        this.isOpen = true;
    }

    public void vote(Candidate candidate)
    {
        if (this.isOpen)
        {
            this.totalVotes++;
            if (this.candidatesManager.getList().contains(candidate))
            {
                this.pool.get(candidate).increment();
            }
            try
            {
                Thread.sleep(3000); // Tempo de feedback do usuário (3s)
            }
            catch (InterruptedException ex)
            {
                Logger.getLogger(ElectionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.candidateNumber = 0;
    }

    public void close()
    {
        this.isOpen = false;
    }

    public Integer getVotes(Candidate candidate)
    {
        if (this.pool.get(candidate) != null)
        {
            return this.pool.get(candidate).total();
        }
        return 0;
    }

    public Integer getPercentage(Candidate candidate)
    {
        if(this.pool.get(candidate) != null)
        {
            return (int) (100.0 * this.pool.get(candidate).total() / this.totalVotes);
        }
        return 0;
    }

    public List<Pair<Candidate, Integer>> getResults()
    {
        List<Pair<Candidate, Integer>> result = new ArrayList<>();
        for (Candidate candidate : this.pool.keySet())
        {
            result.add(new Pair(candidate, this.pool.get(candidate)));
        }
        return result;
    }

    public CandidatesManager getCandidatesManager()
    {
        return candidatesManager;
    }

    public void setCandidatesManager(CandidatesManager candidatesManager)
    {
        this.candidatesManager = candidatesManager;
    }

    public Boolean getIsOpen()
    {
        return isOpen;
    }

    public void setIsOpen(Boolean isOpen)
    {
        this.isOpen = isOpen;
    }

    public Integer getCandidateNumber()
    {
        return candidateNumber;
    }

    public void setCandidateNumber(Integer candidateNumber)
    {
        this.candidateNumber = candidateNumber;
    }

    public Integer getTotalVotes()
    {
        return totalVotes;
    }

    public void setTotalVotes(Integer totalVotes)
    {
        this.totalVotes = totalVotes;
    }
    
    public Boolean alreadyVoted(Voter voter)
    {
        return this.alreadyVoted.contains(voter);
    }
    
    public void setAsVoted(Voter voter)
    {
        this.alreadyVoted.add(voter);
    }

    public List<Voter> getAlreadyVoted()
    {
        return alreadyVoted;
    }

    public void setAlreadyVoted(List<Voter> alreadyVoted)
    {
        this.alreadyVoted = alreadyVoted;
    }
}