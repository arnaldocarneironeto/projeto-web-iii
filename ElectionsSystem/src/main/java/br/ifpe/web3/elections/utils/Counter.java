package br.ifpe.web3.elections.utils;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class Counter
{
    private Integer count;

    public Counter()
    {
        this.count = 0;
    }

    public void increment()
    {
        this.count++;
    }

    public Integer total()
    {
        return this.count;
    }
}