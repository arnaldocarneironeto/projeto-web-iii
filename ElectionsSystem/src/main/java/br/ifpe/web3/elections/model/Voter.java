package br.ifpe.web3.elections.model;

import java.io.Serializable;
import java.util.Objects;

import org.hibernate.validator.constraints.Length;

import br.ifpe.web3.elections.utils.IdGenerator;
import br.ifpe.web3.elections.utils.VoterNumberCreator;
import br.ifpe.web3.elections.model.validators.ValidVoterNumber;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class Voter implements Serializable
{
    private static final long serialVersionUID = -8128294911595253813L;

    private final Integer id;

    @NotBlank(message = "{name_must_be_informed}")
    @Length(max = 40, message = "{no_more_than_40_characters_in_name}")
    private String name;

    @DecimalMin(value = "100000000000", message = "{voter_registration_number_must_be_exactly_12_characters_long}")
    @DecimalMax(value = "999999999999", message = "{voter_registration_number_must_be_exactly_12_characters_long}")
    @ValidVoterNumber(message = "{invalid_voters_number}")
    private Long voterNumber;

    public Voter()
    {
        this.id = IdGenerator.getInstance().getRandomId();
        this.voterNumber = VoterNumberCreator.createValidVoterNumber();
    }

    public Voter(Long voterNumber)
    {
        this();
        this.setVoterNumber(voterNumber);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getVoterNumber()
    {
        return voterNumber;
    }

    public final void setVoterNumber(Long voterNumber)
    {
        this.voterNumber = voterNumber;
    }

    public Integer getId()
    {
        return id;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Voter that = (Voter) obj;
        if (!Objects.equals(this.voterNumber, that.voterNumber))
        {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.voterNumber);
        return hash;
    }
}