package br.ifpe.web3.elections.model;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import br.ifpe.web3.elections.utils.IdGenerator;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class Candidate implements Serializable
{
    private static final long serialVersionUID = -5398368007826789558L;

    private final Integer id;

    @NotEmpty(message = "{name_must_be_informed}")
    @Length(max = 40, message = "{no_more_than_40_characters_in_name}")
    private String name;

    @NotNull(message = "{no_null_number}")
    @Digits(integer = 2, fraction = 0, message = "{invalid_number}")
    @DecimalMin(value = "00", message = "{invalid_number}")
    @DecimalMax(value = "99", message = "{invalid_number}")
    private Integer number;

    public Candidate()
    {
        this.id = IdGenerator.getInstance().getRandomId();
    }

    public Candidate(Integer candidateNumber)
    {
        this();
        this.number = candidateNumber;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getNumber()
    {
        return number;
    }

    public void setNumber(Integer number)
    {
        this.number = number;
    }

    public Integer getId()
    {
        return id;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Candidate other = (Candidate) obj;
        if (!Objects.equals(this.number, other.number))
        {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.number);
        return hash;
    }
}