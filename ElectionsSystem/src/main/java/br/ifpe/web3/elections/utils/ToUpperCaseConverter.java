package br.ifpe.web3.elections.utils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@FacesConverter("toUpperCaseConverter")
public class ToUpperCaseConverter implements Converter
{
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value)
    {
        return value != null? value.toUpperCase(): null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value)
    {
        return value instanceof String? ((String) value).toUpperCase(): null;
    }
}