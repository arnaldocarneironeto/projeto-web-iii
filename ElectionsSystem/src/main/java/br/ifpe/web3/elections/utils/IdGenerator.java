package br.ifpe.web3.elections.utils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public final class IdGenerator
{
    private static final IdGenerator INSTANCE = new IdGenerator();
    private static final Random RND = new SecureRandom();
    private static final List<Integer> IDS = new ArrayList<>();

    private IdGenerator()
    {
    }

    public static IdGenerator getInstance()
    {
        return INSTANCE;
    }

    public Integer getRandomId()
    {
        Integer result;
        do
        {
            result = RND.nextInt();
        }
        while (result < 0 || IDS.contains(result));
        IDS.add(result);
        return result;
    }
}