package br.ifpe.web3.elections.model.validators;

import java.util.ArrayList;
import java.util.List;
import javax.faces.validator.FacesValidator;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@FacesValidator("br.ifpe.web3.elections.voternumbervalidator")
public class VoterNumberValidator implements ConstraintValidator<ValidVoterNumber, Long>
{
    public static Boolean isValid(Long voterNumber)
    {
        if(voterNumber == null)
        {
            return false;
        }

        String voterNumberString = voterNumber.toString();
        int numberOfDigits = voterNumberString.length();
        if (numberOfDigits != 12)
        {
            return false;
        }

        List<Integer> digits = new ArrayList<>();
        for (int i = 0; i < numberOfDigits; ++i)
        {
            digits.add(Integer.parseInt(Mid(voterNumberString, numberOfDigits - (11 - i), 1)));
        }

        int dv1 = 0;
        for (int i = 0; i < 8; ++i)
        {
            dv1 += (i + 2) * digits.get(i);
        }
        dv1 %= 11;
        dv1 = dv1 >= 10 ? dv1 % 10 : dv1;

        int dv2 = ((digits.get(8) * 7 + digits.get(9) * 8 + dv1 * 9) % 11) % 10;

        return digits.get(10) == dv1 && digits.get(11) == dv2;
    }

    private static String Mid(String texto, int inicio, int tamanho)
    {
        String strMid = texto.substring(inicio - 1, inicio + (tamanho - 1));
        return strMid;
    }

    @Override
    public void initialize(ValidVoterNumber constraintAnnotation) {}

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context)
    {
        return isValid(value);
    }
}