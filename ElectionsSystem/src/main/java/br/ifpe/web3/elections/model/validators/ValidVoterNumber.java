package br.ifpe.web3.elections.model.validators;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.*;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.*;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = VoterNumberValidator.class)
@Documented
public @interface ValidVoterNumber
{
    String message() default "ValidationMessages.invalid_voters_number";
    
    Class<?>[] groups() default {};
    
    Class<? extends Payload>[] payload() default {};
}