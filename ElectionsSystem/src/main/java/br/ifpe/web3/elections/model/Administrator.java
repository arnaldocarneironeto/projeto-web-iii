package br.ifpe.web3.elections.model;

import java.io.Serializable;
import java.util.Objects;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import br.ifpe.web3.elections.utils.IdGenerator;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class Administrator implements Serializable
{
    private static final long serialVersionUID = 9192637785643079621L;

    private final Integer id;

    @NotBlank(message = "{name_must_be_informed}")
    @Length(max = 40, message = "{no_more_than_40_characters_in_name}")
    private String name;

    @NotBlank(message = "{no_empty_login}")
    @Length(min = 4, max = 16, message = "{login_must_be_between_4_and_16_characters}")
    private String login;

    @NotBlank(message = "{no_empty_password}")
    @Length(min = 4, max = 16, message = "{passw_must_be_between_4_and_16_characters}")
    private String password;

    public Administrator()
    {
        this.id = IdGenerator.getInstance().getRandomId();
    }

    public Administrator(String login)
    {
        this();
        this.setLogin(login);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getLogin()
    {
        return login;
    }

    public final void setLogin(String login)
    {
        this.login = login;
    }

    public Boolean isValidPassword(String password)
    {
        return this.password.equals(password);
    }

    public String getPassword()
    {
        return this.password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public Integer getId()
    {
        return id;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Administrator other = (Administrator) obj;
        if (!Objects.equals(this.login, other.login))
        {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.login);
        return hash;
    }
}