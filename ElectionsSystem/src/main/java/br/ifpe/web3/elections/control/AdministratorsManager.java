package br.ifpe.web3.elections.control;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

import br.ifpe.web3.elections.model.Administrator;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@ManagedBean(name = "AdministratorsManager")
@ApplicationScoped
public class AdministratorsManager implements Serializable
{
    private static final long serialVersionUID = 8998603563183938282L;
    private List<Administrator> list = new ArrayList<>();
    private Administrator currentadministrator;
    private Boolean editing;

    public AdministratorsManager()
    {
        this.editing = false;
        Administrator defaultAdm = new Administrator();
        defaultAdm.setLogin("admin");
        defaultAdm.setName("Administrator");
        defaultAdm.setPassword("admin");
        this.list.add(defaultAdm);
    }

    public String list()
    {
        return "crud_administrator?faces-redirect=true";
    }

    public void newAdministrator()
    {
        this.currentadministrator = new Administrator();
        this.editing = true;
    }

    public void save()
    {
        if (this.list.contains(this.currentadministrator) == false)
        {
            this.list.add(currentadministrator);
        }
        this.editing = false;
    }

    public void cancel()
    {
        this.editing = false;
    }

    public void update(Administrator administrator)
    {
        this.currentadministrator = administrator;
        this.editing = true;
    }

    public void delete(Administrator administrator)
    {
        this.list.remove(administrator);
    }

    public Administrator getAdminByLogin(String login)
    {
        if (this.list.indexOf(new Administrator(login)) >= 0)
        {
            return this.list.get(this.list.indexOf(new Administrator(login)));
        }
        return null;
    }

    public List<Administrator> getList()
    {
        return list;
    }

    public void setList(List<Administrator> list)
    {
        this.list = list;
    }

    public Administrator getCurrentadministrator()
    {
        return currentadministrator;
    }

    public void setCurrentadministrator(Administrator currentadministrator)
    {
        this.currentadministrator = currentadministrator;
    }

    public Boolean getEditing()
    {
        return editing;
    }

    public void setEditing(Boolean editing)
    {
        this.editing = editing;
    }
}