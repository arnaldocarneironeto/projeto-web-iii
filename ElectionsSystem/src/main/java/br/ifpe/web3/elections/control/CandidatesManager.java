package br.ifpe.web3.elections.control;

import br.ifpe.web3.elections.model.Candidate;
import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@ManagedBean(name = "CandidatesManager")
@ApplicationScoped
public class CandidatesManager implements Serializable
{
    private static final long serialVersionUID = 4188151378531865196L;
    private List<Candidate> list = new ArrayList<>();
    private Candidate currentcandidate;
    private Boolean editing;

    public CandidatesManager()
    {
        this.editing = false;
        addDummyCandidate("Null", -1, this.list);
        addDummyCandidate("White", 0, this.list);
    }
    
    public String list()
    {
        return "crud_candidate?faces-redirect=true";
    }
    
    public void newCandidate()
    {
        this.currentcandidate = new Candidate();
        this.editing = true;
    }

    public void save()
    {
        if (this.list.contains(this.currentcandidate) == false)
        {
            this.list.add(currentcandidate);
        }
        this.editing = false;
    }

    public void cancel()
    {
        this.editing = false;
    }

    public void update(Candidate candidate)
    {
        this.currentcandidate = candidate;
        this.editing = true;

    }

    public void delete(Candidate candidate)
    {
        this.list.remove(candidate);
    }

    public List<Candidate> getList()
    {
        return list;
    }

    public void setList(List<Candidate> list)
    {
        this.list = list;
    }

    public Candidate getCurrentcandidate()
    {
        return currentcandidate;
    }

    public void setCurrentcandidate(Candidate currentcandidate)
    {
        this.currentcandidate = currentcandidate;
    }

    public Boolean getEditing()
    {
        return editing;
    }

    public void setEditing(Boolean editing)
    {
        this.editing = editing;
    }

    private static void addDummyCandidate(String name, Integer number, List<Candidate> candidates)
    {
        Candidate candidate = new Candidate(number);
        candidate.setName(name);
        candidates.add(candidate);
    }
}