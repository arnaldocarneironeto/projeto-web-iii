package br.ifpe.web3.elections.control;

import br.ifpe.web3.elections.model.Voter;
import java.io.Serializable;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@ManagedBean(name = "VoterLoginManager")
@SessionScoped
public class VoterLoginManager implements Serializable
{
    private static final long serialVersionUID = -2481225989373984762L;

    @ManagedProperty(value = "#{VotersManager}")
    private VotersManager votersManager;
    @ManagedProperty(value = "#{ElectionManager}")
    private ElectionManager electionManager;
    private String name;
    private Long voterNumber;
    private Boolean logged;

    public VoterLoginManager()
    {
        this.logoff();
    }

    public void logon()
    {
        if (votersManager.getList().contains(new Voter(this.voterNumber)))
        {
            if(electionManager.alreadyVoted(new Voter(this.voterNumber)))
            {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                ResourceBundle bundle = ResourceBundle.getBundle("ValidationMessages", facesContext.getViewRoot().getLocale());
                facesContext.addMessage(null, new FacesMessage(bundle.getString("already_voted")));
            }
            else
            {
                Voter voter = votersManager.getVoterByNumber(this.voterNumber);
                electionManager.setAsVoted(voter);
                this.name = voter.getName();
                this.voterNumber = voter.getVoterNumber();
                this.logged = true;
            }
        }
        else
        {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ResourceBundle bundle = ResourceBundle.getBundle("ValidationMessages", facesContext.getViewRoot().getLocale());
            facesContext.addMessage(null, new FacesMessage(bundle.getString("voter_number_is_not_yet_registered")));
        }
    }

    public final String logoff()
    {
        this.logged = false;
        this.name = "";
        this.voterNumber = 0L;
        return "index?faces-redirect=true";
    }

    public VotersManager getVotersManager()
    {
        return votersManager;
    }

    public void setVotersManager(VotersManager votersManager)
    {
        this.votersManager = votersManager;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Boolean getLogged()
    {
        return logged;
    }

    public void setLogged(Boolean logged)
    {
        this.logged = logged;
    }

    public Long getVoterNumber()
    {
        return voterNumber;
    }

    public void setVoterNumber(Long voterNumber)
    {
        this.voterNumber = voterNumber;
    }

    public ElectionManager getElectionManager()
    {
        return electionManager;
    }

    public void setElectionManager(ElectionManager electionManager)
    {
        this.electionManager = electionManager;
    }
}